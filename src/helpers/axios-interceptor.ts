import axios, { AxiosError, AxiosRequestConfig } from 'axios';
import { LoginService } from 'src/app/services/login.service';



export const authInterceptor = (loginService: LoginService): void => {
    axios.interceptors.request.use(
        (config: AxiosRequestConfig) => {
            if (config.headers && config.method !== 'get') {
                config.headers['Authorization'] = loginService.getCurrentUserBasicAuthentification()
            }
            return config;
        },
        (error: AxiosError) => {
            console.error('ERROR:', error)
            Promise.reject(error)
        }
    )
}
