import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-create-answer-info-dialog',
  templateUrl: './create-answer-info-dialog.component.html',
  styleUrls: ['./create-answer-info-dialog.component.scss']
})
export class CreateAnswerInfoDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }
  ngOnInit(): void {
  }

}
