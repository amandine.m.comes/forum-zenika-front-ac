import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAnswerInfoDialogComponent } from './create-answer-info-dialog.component';

describe('CreateAnswerInfoDialogComponent', () => {
  let component: CreateAnswerInfoDialogComponent;
  let fixture: ComponentFixture<CreateAnswerInfoDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateAnswerInfoDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAnswerInfoDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
