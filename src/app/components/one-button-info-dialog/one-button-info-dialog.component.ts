import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-one-button-info-dialog',
  templateUrl: './one-button-info-dialog.component.html',
  styleUrls: ['./one-button-info-dialog.component.scss']
})
export class OneButtonInfoDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
  }

}
