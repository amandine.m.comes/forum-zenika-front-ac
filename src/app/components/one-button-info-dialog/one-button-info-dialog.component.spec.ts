import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OneButtonInfoDialogComponent } from './one-button-info-dialog.component';

describe('OneButtonInfoDialogComponent', () => {
  let component: OneButtonInfoDialogComponent;
  let fixture: ComponentFixture<OneButtonInfoDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OneButtonInfoDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OneButtonInfoDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
