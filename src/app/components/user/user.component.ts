import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/models/UserModel';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  constructor() { }

  @Input()
  public user?: User;

  ngOnInit(): void {
  }

}
