import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateAnswerPageComponent } from './pages/create-answer-page/create-answer-page.component';
import { CreateQuestionPageComponent } from './pages/create-question-page/create-question-page.component';
import { CreateUserPageComponent } from './pages/create-user-page/create-user-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { QuestionDetailPageComponent } from './pages/question-detail-page/question-detail-page.component';
import { UserDetailPageComponent } from './pages/user-detail-page/user-detail-page.component';
import { UsersPageComponent } from './pages/users-page/users-page.component';

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'questions/ask', component: CreateQuestionPageComponent },
  { path: 'answers/question/:id', component: QuestionDetailPageComponent },
  { path: 'answers/question/reply/:id', component: CreateAnswerPageComponent },
  { path: 'users/login', component: LoginPageComponent },
  { path: 'users/register', component: CreateUserPageComponent },
  { path: 'users', component: UsersPageComponent },
  { path: 'users/:id', component: UserDetailPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
