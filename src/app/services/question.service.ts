import { Injectable } from '@angular/core';
import axios from 'axios';
import { Question } from '../models/QuestionModel';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  constructor() { }

  apiUrl: string = 'http://localhost:8080/api/questions';

  public questions: Question[] = [];

  public fetchQuestions() {
    axios.get(`${this.apiUrl}`)
      .then((res) => {
        console.log(res.data);
        this.questions = res.data;
      })
      .catch((err => {
        console.log(err);
      }))
  }

  public fetchQuestionsByTitle(title: string) {
    axios.get(`${this.apiUrl}`, {
      params: {
        title: title
      }
    })
      .then((res) => {
        console.log(res.data);
        this.questions = res.data;
      })
      .catch((err) => {
        console.log(err);
      })
  }

  public fetchQuestionsByUser(id: string) {
    axios.get(`${this.apiUrl}/user/${id}`)
      .then((res) => {
        this.questions = res.data;
      })
      .catch((err) => {
        console.log(err);
      })
  }

  public getQuestionById(id: string): Promise<Question> {
    return axios.get(`${this.apiUrl}/id/${id}`);
  }

  public createQuestion(title: string, content: string): Promise<Question> {
    return axios.post(`${this.apiUrl}`,
      {
        title: title,
        content: content,
      })
  }


}
