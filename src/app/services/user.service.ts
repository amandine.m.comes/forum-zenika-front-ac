import { Injectable } from '@angular/core';
import axios from 'axios';
import { User } from '../models/UserModel';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  apiUrl: String = 'http://localhost:8080/api/users';

  public users: User[] = [];

  constructor() { }

  public createUser(email: string, username: string, password: string): Promise<User> {
    return axios.post(`${this.apiUrl}`, {
      email: email,
      username: username,
      password: password
    })
  }

  public fetchUsers() {
    axios.get(`${this.apiUrl}`)
      .then((res) => {
        this.users = res.data;
      })
      .catch((err) => {
        console.log(err);
      })
  }

  public fetchUserById(id: string): Promise<User> {
    return axios.get(`${this.apiUrl}/id/${id}`);
  }

  public searchUserByUsername(username: string) {
    axios.get(`${this.apiUrl}`, {
      params: {
        username: username
      }
    })
      .then((res) => {
        console.log(res.data);
        this.users = res.data;
      })
      .catch((err) => {
        console.log(err);
      })
  }



}
