import { Injectable } from '@angular/core';
import axios from 'axios';
import { Answer } from '../models/AnswerModel';

@Injectable({
  providedIn: 'root'
})
export class AnswerService {

  constructor() { }

  apiUrl: string = 'http://localhost:8080/api/answers';

  public answers: Answer[] = [];

  public getAnswerByQuestion(id: string) {
    axios.get(`${this.apiUrl}/question/${id}`)
      .then((res) => {
        console.log(res);
        this.answers = res.data;
      })
      .catch((err) => {
        console.log(err);
      })
  }

  public createAnswer(id: string, content: string): Promise<Answer> {
    return axios.post(`${this.apiUrl}/${id}`, {
      content: content
    })
  }

  public fetchAnswersByUser(id: string) {
    axios.get(`${this.apiUrl}/user/${id}`)
      .then((res) => {
        this.answers = res.data;
      })
      .catch((err) => {
        console.log(err);
      })
  }


}
