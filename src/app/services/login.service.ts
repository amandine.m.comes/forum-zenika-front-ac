import { Injectable } from '@angular/core';
import axios from 'axios';
import { RegisterUser } from '../models/RegisterUserModel';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private readonly SESSION_STORAGE_KEY = "currentUser";

  apiUrl: String = 'http://localhost:8080/api/users';

  public user?: RegisterUser;

  constructor() { }

  //Vérifier que le user existe dans la base
  public verify(): Promise<any> {
    return axios.post(`${this.apiUrl}/me`);
  }

  //Injection des headers dans l'interceptor
  public getCurrentUserBasicAuthentification(): string {
    const currentUserPlain = sessionStorage.getItem(this.SESSION_STORAGE_KEY)
    console.log(currentUserPlain);
    if (currentUserPlain) {
      const currentUser = JSON.parse(currentUserPlain)
      console.log(currentUser);
      return "Basic " + btoa(currentUser.username + ":" + currentUser.password);
    } else {
      return "";
    }
  }

  //Se connecter
  public login(user: RegisterUser): void {
    sessionStorage.setItem(this.SESSION_STORAGE_KEY, JSON.stringify(user))
    console.log(user);
  }

  //Se déconnecter
  public logout(): void {
    sessionStorage.removeItem(this.SESSION_STORAGE_KEY)
  }

  //Vérifier si un user est connecté
  public isLoggedIn(): boolean {
    if (sessionStorage.getItem(this.SESSION_STORAGE_KEY)) {
      return true;
    } else {
      return false;
    }
  }


}
