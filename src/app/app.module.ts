import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatDialogModule } from '@angular/material/dialog';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { QuestionComponent } from './components/question/question.component';
import { CreateQuestionPageComponent } from './pages/create-question-page/create-question-page.component';
import { FormsModule } from '@angular/forms';
import { QuestionDetailPageComponent } from './pages/question-detail-page/question-detail-page.component';
import { AnswerComponent } from './components/answer/answer.component';
import { CreateAnswerPageComponent } from './pages/create-answer-page/create-answer-page.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { CreateUserPageComponent } from './pages/create-user-page/create-user-page.component';
import { UsersPageComponent } from './pages/users-page/users-page.component';
import { UserComponent } from './components/user/user.component';
import { UserDetailPageComponent } from './pages/user-detail-page/user-detail-page.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InfoDialogComponent } from './components/info-dialog/info-dialog.component';
import { OneButtonInfoDialogComponent } from './components/one-button-info-dialog/one-button-info-dialog.component';
import { HeaderComponent } from './components/header/header.component';
import { CreateAnswerInfoDialogComponent } from './components/create-answer-info-dialog/create-answer-info-dialog.component';


@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    QuestionComponent,
    CreateQuestionPageComponent,
    QuestionDetailPageComponent,
    AnswerComponent,
    CreateAnswerPageComponent,
    LoginPageComponent,
    CreateUserPageComponent,
    UsersPageComponent,
    UserComponent,
    UserDetailPageComponent,
    InfoDialogComponent,
    OneButtonInfoDialogComponent,
    HeaderComponent,
    CreateAnswerInfoDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MatDialogModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
