import { User } from "./UserModel";

export interface Question {
    id: string,
    date: string,
    title: string,
    content: string,
    user: User,
}
