import { Question } from "./QuestionModel";
import { User } from "./UserModel";

export interface Answer {
    id: string,
    answerDate: string,
    content: string,
    question: Question,
    user: User,
}
