import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { QuestionService } from 'src/app/services/question.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  public search: string = '';

  constructor(public questionService: QuestionService) { }

  onKeyupSearch(): void {
    if (this.search.length >= 3) {
      this.questionService.fetchQuestionsByTitle(this.search);
    } else if (this.search.length < 1) {
      this.questionService.fetchQuestions();
    }

  }

  ngOnInit(): void {
    this.questionService.fetchQuestions();
  }

}
