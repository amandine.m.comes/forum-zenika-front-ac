import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-users-page',
  templateUrl: './users-page.component.html',
  styleUrls: ['./users-page.component.scss']
})
export class UsersPageComponent implements OnInit {

  public search: string = '';

  constructor(public userService: UserService) { }


  onKeyupSearchUsers(): void {
    if (this.search.length >= 3) {
      this.userService.searchUserByUsername(this.search);
    } else if (this.search.length < 1) {
      this.userService.fetchUsers();
    }
  }

  ngOnInit(): void {
    this.userService.fetchUsers();
  }

}
