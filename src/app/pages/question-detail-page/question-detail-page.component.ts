import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Question } from 'src/app/models/QuestionModel';
import { AnswerService } from 'src/app/services/answer.service';
import { LoginService } from 'src/app/services/login.service';
import { QuestionService } from 'src/app/services/question.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-question-detail-page',
  templateUrl: './question-detail-page.component.html',
  styleUrls: ['./question-detail-page.component.scss']
})
export class QuestionDetailPageComponent implements OnInit {


  constructor(private route: ActivatedRoute, public answerService: AnswerService, public questionService: QuestionService, public userService: UserService, public loginService: LoginService) { }

  public question?: Question;

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      //trouver la question par son id
      this.questionService.getQuestionById(params['id']).then((question: any) => {
        this.question = question.data;
      })
      //trouver les réponses à la question
      this.answerService.getAnswerByQuestion(params['id']);
    })
  }

}
