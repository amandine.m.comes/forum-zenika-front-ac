import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/models/UserModel';
import { AnswerService } from 'src/app/services/answer.service';
import { QuestionService } from 'src/app/services/question.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-detail-page',
  templateUrl: './user-detail-page.component.html',
  styleUrls: ['./user-detail-page.component.scss']
})
export class UserDetailPageComponent implements OnInit {

  public user: User[] = [];

  constructor(public activatedRoute: ActivatedRoute, public userService: UserService, public questionService: QuestionService, public answerService: AnswerService) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      //retrouver le user par son id
      this.userService.fetchUserById(params['id'])
        .then((user: any) => {
          this.user = user.data;
        })
        .catch((err) => {
          console.log(err);
        })
      //afficher les questions du user
      this.questionService.fetchQuestionsByUser(params['id']);
      //afficher les réponses du user
      this.answerService.fetchAnswersByUser(params['id']);
    })
  }

}
