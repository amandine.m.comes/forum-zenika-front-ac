import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAnswerPageComponent } from './create-answer-page.component';

describe('CreateAnswerPageComponent', () => {
  let component: CreateAnswerPageComponent;
  let fixture: ComponentFixture<CreateAnswerPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateAnswerPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAnswerPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
