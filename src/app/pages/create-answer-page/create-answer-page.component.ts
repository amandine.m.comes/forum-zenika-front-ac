import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { Question } from 'src/app/models/QuestionModel';
import { AnswerService } from 'src/app/services/answer.service';
import { QuestionService } from 'src/app/services/question.service';
import { InfoDialogComponent } from 'src/app/components/info-dialog/info-dialog.component';
import { CreateAnswerInfoDialogComponent } from 'src/app/components/create-answer-info-dialog/create-answer-info-dialog.component';


@Component({
  selector: 'app-create-answer-page',
  templateUrl: './create-answer-page.component.html',
  styleUrls: ['./create-answer-page.component.scss']
})
export class CreateAnswerPageComponent implements OnInit {

  constructor(public answerService: AnswerService, public questionService: QuestionService,
    public activatedRoute: ActivatedRoute, public router: Router, public matDialog: MatDialog) { }

  public content: string = '';
  public id: string = '';
  private wordsTab: string[] = [];
  public question?: Question;

  onSubmitCreateAnswer(): void {
    this.wordsTab = this.content.split(" ");
    if (this.wordsTab.length <= 100) {
      this.answerService.createAnswer(this.id, this.content)
        .then(() => {
          this.openDialogOneButton("Votre réponse a bien été créée.", "Ok");
          this.router.navigate(['/answers/question', this.id])
        })
        .catch((err) => {
          console.log(err);
          this.openDialogTwoButtons("Oups, vous devez être connecté pour envoyer une réponse.", "Se connecter", "Abandonner");
        })
    } else {
      this.openDialogTwoButtons("Votre réponse ne doit pas dépasser 100 mots.", "Ok", "Abandonner");
    }

  }

  openDialogTwoButtons(message: string, closeButton: string, goToHomeButton: string) {
    const dialogRef = this.matDialog.open(InfoDialogComponent, {
      width: '250px',
      data: { message, closeButton, goToHomeButton }
    });
  }

  openDialogOneButton(message: string, closeButton: string) {
    const dialogRef = this.matDialog.open(CreateAnswerInfoDialogComponent, {
      width: '250px',
      data: { message, closeButton }
    });
  }


  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id']
      this.questionService.getQuestionById(this.id)
        .then((question: any) => {
          this.question = question.data;
        });
    })
  }

}


