import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { QuestionService } from 'src/app/services/question.service';
import { InfoDialogComponent } from 'src/app/components/info-dialog/info-dialog.component';
import { OneButtonInfoDialogComponent } from 'src/app/components/one-button-info-dialog/one-button-info-dialog.component';


@Component({
  selector: 'app-create-question-page',
  templateUrl: './create-question-page.component.html',
  styleUrls: ['./create-question-page.component.scss']
})
export class CreateQuestionPageComponent implements OnInit {


  constructor(public questionService: QuestionService, public matDialog: MatDialog) { }

  private wordsTab: string[] = [];
  private numberOfWords: number = 0;

  public title: string = '';
  public content: string = '';


  onSubmitCreateQuestion(): void {
    this.wordsTab = this.title.split(" ");
    this.numberOfWords = this.wordsTab.length;

    if (this.numberOfWords > 20) {
      this.openDialogTwoButtons("Le titre de la question ne doit pas dépasser 20 mots.", "Ok", "Abandonner");
    } else if (!this.wordsTab[this.numberOfWords - 1].includes("?")) {
      this.openDialogTwoButtons("Le titre de la question doit se terminer par '?'.", "Ok", "Abandonner");
    } else {
      this.questionService.createQuestion(this.title, this.content)
        .then(() => {
          this.openDialogOneButton("Votre question a bien été créée.", "Ok");
        })
        .catch((error) => {
          console.log(error);
          this.openDialogTwoButtons("Oups, vous devez être connecté pour poser une question.", "Se connecter", "Abandonner");
        })
    }

  }
  openDialogOneButton(message: string, goToHomeButton: string) {
    const dialogRef = this.matDialog.open(OneButtonInfoDialogComponent, {
      width: '250px',
      data: { message, goToHomeButton }
    });
  }

  openDialogTwoButtons(message: string, closeButton: string, goToHomeButton: string) {
    const dialogRef = this.matDialog.open(InfoDialogComponent, {
      width: '250px',
      data: { message, closeButton, goToHomeButton }
    });
  }


  ngOnInit(): void {
  }

}
