import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { InfoDialogComponent } from 'src/app/components/info-dialog/info-dialog.component';
import { OneButtonInfoDialogComponent } from 'src/app/components/one-button-info-dialog/one-button-info-dialog.component';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  public username: string = "";
  public password: string = "";

  constructor(private loginService: LoginService, private router: Router, public matDialog: MatDialog) { }

  onSubmitLogin() {
    this.loginService.login({ username: this.username, password: this.password });
    this.loginService.verify()
      .then(() => {
        this.openDialogOneButton("Vous êtes bien connecté!", "Ok")
      })
      .catch((err) => {
        console.log(err);
        this.loginService.logout();
        this.openDialogTwoButtons("Oups, le pseudo ou le mot de passe n'est pas correct...", "Réessayer", "Abandonner")
      })
  }

  openDialogOneButton(message: string, goToHomeButton: string) {
    const dialogRef = this.matDialog.open(OneButtonInfoDialogComponent, {
      width: '250px',
      data: { message, goToHomeButton }
    });
  }

  openDialogTwoButtons(message: string, closeButton: string, goToHomeButton: string) {
    const dialogRef = this.matDialog.open(InfoDialogComponent, {
      width: '250px',
      data: { message, closeButton, goToHomeButton }
    });
  }


  ngOnInit(): void {
  }

}
