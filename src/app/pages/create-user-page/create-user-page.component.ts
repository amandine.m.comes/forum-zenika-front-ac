import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { InfoDialogComponent } from 'src/app/components/info-dialog/info-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { OneButtonInfoDialogComponent } from 'src/app/components/one-button-info-dialog/one-button-info-dialog.component';

@Component({
  selector: 'app-create-user-page',
  templateUrl: './create-user-page.component.html',
  styleUrls: ['./create-user-page.component.scss']
})
export class CreateUserPageComponent implements OnInit {

  public email: string = '';
  public username: string = '';
  public password: string = '';

  constructor(private userService: UserService, public matDialog: MatDialog) { }

  onSubmitCreateUser(): void {
    this.userService.createUser(this.email, this.username, this.password)
      .then((res) => {
        console.log(res);
        this.openDialogOneButton("Votre compte a bien été créé!", "Super!")
      })
      .catch((err) => {
        console.log(err);
        this.openDialogTwoButtons("Oups, ces identifiants existent déjà...", "Réessayer", "Abandonner")
      })
  }

  openDialogOneButton(message: string, goToHomeButton: string) {
    const dialogRef = this.matDialog.open(OneButtonInfoDialogComponent, {
      width: '250px',
      data: { message, goToHomeButton }
    });
  }

  openDialogTwoButtons(message: string, closeButton: string, goToHomeButton: string) {
    const dialogRef = this.matDialog.open(InfoDialogComponent, {
      width: '250px',
      data: { message, closeButton, goToHomeButton }
    });
  }


  ngOnInit(): void {
  }

}
