import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { authInterceptor } from 'src/helpers/axios-interceptor';
import { errorInterceptor } from 'src/helpers/error-interceptor';
import { LoginService } from './services/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'forum-zenika-front';

  constructor(private loginService: LoginService, private router: Router) {
    authInterceptor(this.loginService),
      errorInterceptor(this.loginService, this.router)
  }
}
